<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<div class="container">
    <div class="row justify-content-center">
        <div class="jumbotron">
            <h1 class="display-4">Oops...</h1>
            <p class="lead">There was an error!</p>
            <hr class="my-4">
            <p>Go back to Home Page</p>
            <p class="lead">
                <?php
                if (isset($_SESSION['email'])) {
                    echo "<a class='navbar-brand' href='" . site_url('dash') . "'>Home Page</a>";
                } else {
                    echo "<a class='navbar-brand' href='" . site_url() . "'>Home Page</a>";
                }
                ?>
            </p>
        </div>
    </div>
</div>
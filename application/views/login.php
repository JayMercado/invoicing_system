<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<div class="container mt-5">
  <div class="row justify-content-center">
    <div class="col-md-6">
      <h3>Login</h3>
      <div class="card">
        <div class="card-header">Login Details</div>
        <div class="card-body">
          <?= form_open('home/login_process'); ?>
          <div class="form-group">
            <label for="email">Email address</label>
            <input name="email" type="email" class="form-control" id="email" placeholder="Enter email" required>
          </div>
          <div class="form-group">
            <label for="password">Password</label>
            <input name="password" type="password" class="form-control" id="password" placeholder="Password" required>
          </div>
          <input name="login" value="Login" type="submit" class="btn btn-outline-primary btn-block">
          <p class="mt-1">Don't have an account? <a href="<?= site_url('home/register'); ?>">Register</a></p>
          <?= form_close(); ?>
        </div>
      </div>
    </div>
  </div>
</div>
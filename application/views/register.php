<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<div class="container mt-5">
  <div class="row justify-content-center">
    <div class="col-md-6">
      <h3>Register</h3>
      <div class="card">
        <div class="card-header">Register</div>
        <div class="card-body">
          <?php echo form_open('home/register_process'); ?>
          <div class="form-group">
            <label for="email">Email address</label>
            <input name="email" type="email" class="form-control" id="email" placeholder="Enter email" required>
          </div>
          <div class="form-group">
            <label for="password">Password</label>
            <input name="password" type="password" class="form-control" id="password" placeholder="Password" required>
          </div>
          <input name="register" value="Register" type="submit" class="btn btn-outline-primary btn-block">
          <p class="mt-1">Already have an account? <a href="<?= site_url('home'); ?>">Login</a></p>
          <?php echo form_close(); ?>
        </div>
      </div>
    </div>
  </div>
</div>
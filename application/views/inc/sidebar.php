<div class="card mt-3">
    <div class="card-heading pl-3 py-1">Invoices</div>
    <div class="list-group">
        <a href="<?= site_url();?>/InvoiceController" class="list-group-item">Add Invoice</a>
        <a href="<?= site_url();?>/InvoiceController/view_invoices" class="list-group-item">Invoice List</a>
    </div>
</div>
<div class="card mt-3">
    <div class="card-heading pl-3 py-1">Clients</div>
    <div class="list-group">
        <a href="<?= site_url();?>/ClientController" class="list-group-item">Add Client</a>
        <a href="<?= site_url();?>/ClientController/view_clients" class="list-group-item">Client List</a>
    </div>
</div>
<div class="card mt-3">
    <div class="card-heading pl-3 py-1">Product</div>
    <div class="list-group">
        <a href="<?= site_url();?>/ProductController" class="list-group-item">Add Product</a>
        <a href="<?= site_url();?>/ProductController/view_products" class="list-group-item">Product List</a>
    </div>
</div>
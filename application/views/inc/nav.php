<div class="container">
    <nav class="navbar navbar-expand-lg navbar-light bg-light" style="background-color: white!important;">
        <?php
        if (isset($_SESSION['email'])) {
            echo "<a class='navbar-brand' href='" . site_url('dash') . "'><img src='".base_url()."billease.svg' height='50' width='100'></a>";
        } else {
            echo "<a class='navbar-brand' href='" . site_url() . "'><img src='".base_url()."billease.svg' height='50' width='100'></a>";
        }
        ?>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse justify-content-end" id="navbarSupportedContent">
            <ul class="navbar-nav">

                <?php
                if (isset($_SESSION['email'])) {
                    echo "<li class='nav-item'><a class='nav-link' href='" . site_url() . "/home/logout'>Logout</a></li>";
                } else {
                    echo "<li class='nav-item'><a class='nav-link' href='" . site_url() . "/home/login'>Login</a></li>";
                    echo "<li class='nav-item'><a class='nav-link' href='" . site_url() . "/home/register'>Register</a></li>";
                }
                ?>


            </ul>
        </div>
    </nav>
</div>
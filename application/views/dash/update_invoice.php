<?php
defined('BASEPATH') or exit('No direct script access allowed');
if (!$_SESSION['email']) {
    redirect('home', 'refresh');
}
$invoice_number = $this->uri->segment(3);
?>
<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title>Update Invoice</title>
</head>

<body>
    <?php $this->load->view('inc/nav'); ?>

    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <!-- sidebar -->
                <?php $this->load->view('inc/sidebar'); ?>
            </div>
            <div class="col-md-9">
                <div class="card mt-3">
                    <div class="card-header"><strong>Update Invoice</strong></div>
                    <div class="card-body">
                        <?= form_open('InvoiceController/update_invoice_process/' . $invoice_number); ?>
                        <?php
                        $invoice_list = $this->db->get_where('invoices', array('invoice_number' => $invoice_number));
                        foreach ($invoice_list->result() as $invoice) { ?>
                            <div class="form-group">
                                <label for="invoice_client">Client</label>
                                <select name="invoice_client" id="invoice_client" class="form-control">
                                    <?php
                                    $this->db->order_by("client_id", "asc");
                                    $client_list = $this->db->get('clients');
                                    foreach ($client_list->result() as $client) { ?>
                                        <option value="<?= $client->client_name ?>"><?= $client->client_name ?></option>
                                    <?php } ?>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="invoice_payment_due">Payment Due</label>
                                <input name="invoice_payment_due" type="date" class="form-control" id="invoice_payment_due" value="<?= $invoice->invoice_payment_due; ?>" required>
                            </div>

                            <div class="form-group">
                                <label for="invoice_status">Status</label>
                                <select name="invoice_status" id="invoice_status" class="form-control">
                                    <option value="Unpaid">Unpaid</option>
                                    <option value="Paid">Paid</option>
                                    <option value="Overdue">Overdue</option>
                                </select>
                            </div>
                        <?php } ?>
                        <div class="form-group row">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th scope="col" class="text-center">Product</th>
                                        <th scope="col" class="text-center">Price</th>
                                        <th scope="col" class="text-center">Quantity</th>
                                        <th scope="col" class="text-center">Amount</th>
                                    </tr>
                                </thead>
                                <tbody id="products_list">
                                    <?php
                                    $this->db->where('invoice_id', $invoice_number);
                                    $list_items = $this->db->get('line_items');
                                    $list_items = $list_items->result();
                                    foreach ($list_items as $list_item) {
                                    ?>
                                        <tr>
                                            <td class="text-center"><?= $list_item->product_name; ?></td>
                                            <td class="text-right">P<?= number_format($list_item->product_price, 2); ?></td>
                                            <td class="text-center"><?= $list_item->product_quantity; ?></td>
                                            <td class="text-right">P<?=number_format($list_item->product_quantity * $list_item->product_price, 2); ?></td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                        <input type="submit" name="update_invoice" value="Update Invoice" class="btn btn-outline-primary btn-block mt-3">
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>

</html>
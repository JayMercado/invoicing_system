<?php
defined('BASEPATH') or exit('No direct script access allowed');
if (!$_SESSION['email']) {
    redirect('home', 'refresh');
}
?>
<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title>Hello, world!</title>
</head>

<body>
    <?php $this->load->view('inc/nav'); ?>

    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <!-- sidebar -->
                <?php $this->load->view('inc/sidebar'); ?>
            </div>
            <div class="col-md-9">
                <?= form_open('Dash/', ['method' => 'GET']); ?>
                <div class="form-group row">
                    <label for="start_date" class="col-md-2 col-form-label">Start Date</label>
                    <div class="col-md-10">
                        <input type="date" name="start_date" class="form-control" id="start_date" value="<?= $_GET['start_date'] ?? '' ?>" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="end_date" class="col-md-2 col-form-label">End Date</label>
                    <div class="col-md-10">
                        <input type="date" name="end_date" class="form-control" id="end_date" value="<?= $_GET['end_date'] ?? '' ?>"  required>
                    </div>
                </div>
                <input type="submit" value="Filter" class="btn btn-outline-primary btn-block">
                <?php echo form_close(); ?>
                <?php
                if(isset($_GET['start_date']) && isset($_GET['end_date'])){
                    $this->db->where('DATE(invoice_date) >=', $_GET['start_date']);
                    $this->db->where('DATE(invoice_date) <=', $_GET['end_date']);
                    $invoice_list = $this->db->get('invoices')->result();
                    $amount = array_map(function ($i) {
                        $line_items = $this->db->where('invoice_id', $i->invoice_number)->get('line_items')->result();
                        $li_amount = array_map(function ($li) {
                            return $li->product_price * $li->product_quantity;
                        }, $line_items);
                        return array_sum($li_amount);
                    }, $invoice_list);
                } else {
                    $invoice_list = $this->db->get('invoices')->result();
                    $amount = array_map(function ($i) {
                        $line_items = $this->db->where('invoice_id', $i->invoice_number)->get('line_items')->result();
                        $li_amount = array_map(function ($li) {
                            return $li->product_price * $li->product_quantity;
                        }, $line_items);
                        return array_sum($li_amount);
                    }, $invoice_list);
                }
                ?>
                <h2>Total Sales Per Day: <?= number_format(array_sum($amount)); ?></h2>

                <?php
                if(isset($_GET['start_date']) && isset($_GET['end_date'])){
                    $this->db->where('YEAR(invoice_date) >=', date('Y', strtotime($_GET['start_date'])));
                    $this->db->where('YEAR(invoice_date) <=', date('Y', strtotime($_GET['end_date'])));
                    $this->db->where('MONTH(invoice_date) >=', date("m",strtotime($_GET['start_date'])));
                    $this->db->where('MONTH(invoice_date) <=', date("m",strtotime($_GET['end_date'])));
                    $invoice_list = $this->db->get('invoices')->result();
                    $amount = array_map(function ($i) {
                        $line_items = $this->db->where('invoice_id', $i->invoice_number)->get('line_items')->result();
                        $li_amount = array_map(function ($li) {
                            return $li->product_price * $li->product_quantity;
                        }, $line_items);
                        return array_sum($li_amount);
                    }, $invoice_list);
                } else {
                    $invoice_list = $this->db->get('invoices')->result();
                    $amount = array_map(function ($i) {
                        $line_items = $this->db->where('invoice_id', $i->invoice_number)->get('line_items')->result();
                        $li_amount = array_map(function ($li) {
                            return $li->product_price * $li->product_quantity;
                        }, $line_items);
                        return array_sum($li_amount);
                    }, $invoice_list);
                }
                ?>
                <h2>Total Sales Per Month: <?= number_format(array_sum($amount)); ?></h2>

                <?php
                if(isset($_GET['start_date']) && isset($_GET['end_date'])){
                    $this->db->where('YEAR(invoice_date) >=', date('Y', strtotime($_GET['start_date'])));
                    $this->db->where('YEAR(invoice_date) <=', date('Y', strtotime($_GET['end_date'])));
                    $invoice_list = $this->db->get('invoices')->result();
                    $amount = array_map(function ($i) {
                        $line_items = $this->db->where('invoice_id', $i->invoice_number)->get('line_items')->result();
                        $li_amount = array_map(function ($li) {
                            return $li->product_price * $li->product_quantity;
                        }, $line_items);
                        return array_sum($li_amount);
                    }, $invoice_list);
                } else {
                    $invoice_list = $this->db->get('invoices')->result();
                    $amount = array_map(function ($i) {
                        $line_items = $this->db->where('invoice_id', $i->invoice_number)->get('line_items')->result();
                        $li_amount = array_map(function ($li) {
                            return $li->product_price * $li->product_quantity;
                        }, $line_items);
                        return array_sum($li_amount);
                    }, $invoice_list);
                }
                ?>
                <h2>Total Sales Per Year: <?= number_format(array_sum($amount)); ?></h2>
            </div>
        </div>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>

</html>
<?php
defined('BASEPATH') or exit('No direct script access allowed');
if (!$_SESSION['email']) {
    redirect('home', 'refresh');
}
$client_id = $this->uri->segment(3);
?>
<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title>Update Client</title>
</head>

<body>
    <?php $this->load->view('inc/nav'); ?>

    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <!-- sidebar -->
                <?php $this->load->view('inc/sidebar'); ?>
            </div>
            <div class="col-md-9">
                <div class="card mt-3">
                    <div class="card-header"><strong>Update Client</strong></div>
                    <div class="card-body">
                        <?= form_open('ClientController/update_client_process/'.$client_id); ?>
                        <?php
                        $client_list = $this->db->get_where('clients', array('client_id' => $client_id));
                        foreach($client_list->result() as $client) { ?>
                        <div class="form-group">
                            <label for="client_name">Client Name</label>
                            <input type="text" name="client_name" class="form-control" id="client_name" value="<?=$client->client_name;?>" required>
                        </div>

                        <div class="form-group">
                            <label for="contact_number">Contact Number</label>
                            <input type="number" name="contact_number" class="form-control" id="contact_number" value="<?=$client->contact_number;?>" required>
                        </div>

                        <div class="form-group">
                        <label for="address">Address</label>
                            <input type="text" name="address" class="form-control" id="address" placeholder="Address" value="<?=$client->address;?>" required>
                        </div>
                        <?php } ?>
                        <input type="submit" name="update_client" value="Update Client" class="btn btn-outline-primary btn-block">
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>

</html>
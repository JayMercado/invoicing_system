<?php
defined('BASEPATH') or exit('No direct script access allowed');
if (!$_SESSION['email']) {
    redirect('home', 'refresh');
}
?>
<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title>Hello, world!</title>
</head>

<body>
    <?php $this->load->view('inc/nav'); ?>

    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <!-- sidebar -->
                <?php $this->load->view('inc/sidebar'); ?>
            </div>
            <div class="col-md-9">
                <div class="card mt-3">
                    <div class="card-header"><strong>Add Product</strong></div>
                    <div class="card-body">
                        <?= form_open('productController/add_product'); ?>
                        <div class="form-group row">
                            <label for="product_name" class="col-md-2 col-form-label">Product Name</label>
                            <div class="col-md-10">
                                <input type="text" name="product_name" class="form-control" id="product_name" placeholder="Product Name" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="product_price" class="col-md-2 col-form-label">Price</label>
                            <div class="col-md-10">
                                <input name="product_price" type="number" class="form-control" id="product_price" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="product_quantity" class="col-md-2 col-form-label">Quantity</label>
                            <div class="col-md-10">
                                <input name="product_quantity" type="number" class="form-control" id="product_quantity" required>
                            </div>
                        </div>
                        <input type="submit" name="add_product" value="Add Product" class="btn btn-outline-primary btn-block">
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>

</html>
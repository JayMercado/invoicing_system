<?php
defined('BASEPATH') or exit('No direct script access allowed');
if (!$_SESSION['email']) {
    redirect('home', 'refresh');
}
?>
<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title>Add Invoice</title>
</head>

<body>
    <?php $this->load->view('inc/nav'); ?>

    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <!-- sidebar -->
                <?php $this->load->view('inc/sidebar'); ?>
            </div>
            <div class="col-md-9">
                <div class="card mt-3">
                    <div class="card-header"><strong>Add Invoice</strong></div>
                    <div class="card-body">

                        <div class="form-group row">
                            <label for="invoice_client" class="col-md-2 col-form-label">Client</label>
                            <div class="col-md-10">
                                <select name="invoice_client" id="invoice_client" class="form-control">
                                    <?php
                                    $this->db->order_by("client_id", "asc");
                                    $client_list = $this->db->get('clients');
                                    foreach ($client_list->result() as $client) { ?>
                                        <option value="<?= $client->client_name ?>"><?= $client->client_name ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="invoice_payment_due" class="col-md-2 col-form-label">Payment Due</label>
                            <div class="col-md-10">
                                <input name="invoice_payment_due" type="date" class="form-control" id="invoice_payment_due" required value="2021-02-14">
                            </div>
                        </div>

                        <div class="form-group row">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th scope="col">Product</th>
                                        <th scope="col">Price</th>
                                        <th scope="col">Quantity</th>
                                        <th scope="col">Amount</th>
                                    </tr>
                                </thead>
                                <tbody id="products_list">

                                </tbody>
                            </table>
                            <div class="text-right">
                                <button class="btn btn-primary" id="add_product" type="button">Add Product</button>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="total_amount" class="col-md-2 col-form-label">Total Amount</label>
                            <div class="col-md-10">
                                <input name="total_amount" type="text" class="form-control" id="total_amount" required disabled>
                            </div>
                        </div>

                        <button class="btn btn-outline-primary btn-block" type="button" id="add_invoice">Add Invoice</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

    <!-- AJAX -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>

    <script>
        <?php
        $this->db->order_by("product_id", "asc");
        $product_list = $this->db->get('products');
        ?>
        const PRODUCTS = JSON.parse('<?= json_encode($product_list->result()) ?>');
        $(document).ready(function() {

            $('#add_product').click(() => {
                let tr = '<tr>';
                tr += '<td><select class="form-control product-select"></select></td>';
                tr += '<td><div class="form-control product-price disabled bg-light"></div></td>';
                tr += '<td><input type="number" class="form-control product-quantity" min="1" value="1" required></td>';
                tr += '<td><div class="form-control product-amount bg-light"></div></td>';
                tr += '</tr>';
                let trEl = $(tr);
                addProductsToSelect(trEl);
                $('#products_list').append(trEl);
                updateTotal();
            });

            $(document).on('change', '.product-select', function(e) {
                let select = $(e.currentTarget);
                let product_name = e.currentTarget.value;
                let product = PRODUCTS.find(x => x.product_name == product_name);
                select.parent().parent().find('.product-price')[0].textContent = product.product_price;
                let amount = product.product_price * select.parent().parent().find('.product-quantity')[0].value;
                select.parent().parent().find('.product-amount')[0].textContent = amount;
                updateTotal();
            });

            $(document).on('keyup', '.product-quantity', function(e) {
                let product_price = $(e.currentTarget).parent().parent().find('.product-price')[0].textContent;
                let product_amount = $(e.currentTarget).parent().parent().find('.product-amount')[0];
                product_amount.textContent = parseInt(product_price) * e.target.value;
                updateTotal();
            });

            function updateTotal() {
                let amount = 0;
                $('.product-amount').each((i, el) => {
                    amount += parseInt(el.textContent);
                });
                $('#total_amount').val(amount);

            }

            function addProductsToSelect(trEl) {
                let select = trEl.find('.product-select')[0];
                PRODUCTS.forEach(product => {
                    let option = document.createElement('option');
                    option.value = product.product_name;
                    option.dataset.price = product.product_price;
                    option.text = product.product_name;
                    select.add(option);
                });
                select.value = PRODUCTS[0].product_name;
                let input_price = trEl.find('.product-price')[0];
                let input_amount = trEl.find('.product-amount')[0];
                input_price.textContent = input_amount.textContent = PRODUCTS[0].product_price;
            }
            
            $('#add_invoice').click(() => {
                let data = {
                    line_items: [],
                    add_invoice: true
                };
                data.invoice_client = $('#invoice_client').val();
                data.invoice_payment_due = $('#invoice_payment_due').val();
                $('#products_list tr').each((i, el) => {
                    let tr = $(el);
                    let lineItem = {};
                    lineItem.product_name = tr.find('.product-select')[0].value;
                    lineItem.quantity = tr.find('.product-quantity')[0].value;
                    data.line_items.push(lineItem);
                });
                $.post('/invoicing_system/index.php/InvoiceController/add_invoice', data).then(response => {
                    window.location.href = "/invoicing_system/index.php/InvoiceController/view_invoices";
                });
            });
        });
    </script>
</body>

</html>
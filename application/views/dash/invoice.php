<?php
defined('BASEPATH') or exit('No direct script access allowed');
if (!$_SESSION['email']) {
    redirect('home', 'refresh');
}
$invoice_number = $this->uri->segment(3);
?>
<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title>Add Invoice</title>
</head>

<body>
    <?php $this->load->view('inc/nav'); ?>

    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <!-- sidebar -->
                <?php $this->load->view('inc/sidebar'); ?>
            </div>
            <div class="col-md-9">
                <div class="mt-3">
                    <h2 class='text-center'>Invoice</h2>
                    <?php
                    $invoice_list = $this->db->get_where('invoices', array('invoice_number' => $invoice_number));
                    foreach ($invoice_list->result() as $invoice) { ?>
                        <div class="row mt-3">
                            <div class="col-sm-8"></div>
                            <table class="col-sm-4 text-left">
                                <tbody>
                                    <tr>
                                        <td><strong>Invoice Number:</strong></td>
                                        <td><?= $invoice->invoice_number; ?></td>
                                    </tr>
                                    <tr>
                                        <td><strong>Date:</strong></td>
                                        <td><?= date("m-d-Y", strtotime($invoice->invoice_date)); ?></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="row mt-5">
                            <div class="col-sm-6">
                                <div class="row">
                                    <div class="col-sm-3"><strong>From: </strong></div>
                                    <div class="col-sm-9">
                                        <p>Company XYZ</p>
                                        <p>09672587714</p>
                                        <p>Lipa City, Batangas, Philippines</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="row">
                                    <div class="col-sm-3"><strong>To: </strong></div>
                                    <div class="col-sm-9">
                                        <?php
                                        $client_list = $this->db->get_where('clients', array('client_name' => $invoice->invoice_client));
                                        foreach ($client_list->result() as $client) { ?>
                                            <p><?= $client->client_name; ?></p>
                                            <p><?= $client->contact_number; ?></p>
                                            <p><?= $client->address; ?></p>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-3">
                            <div class="col-sm-6">
                                <span><strong>Due: </strong><?=date("m-d-Y", strtotime($invoice->invoice_payment_due)); ?></span>
                            </div>
                        </div>
                        <div class="row mt-3">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th scope="col" class="text-center">Product</th>
                                        <th scope="col" class="text-center">Price</th>
                                        <th scope="col" class="text-center">Quantitity</th>
                                        <th scope="col" class="text-center">Amount</th>
                                    </tr>
                                    <?php

                                    $this->db->where('invoice_id', $invoice_number);
                                    $list = $this->db->get('line_items');
                                    $list_item = $list->result();
                                    $totalAmount = 0;

                                    foreach ($list_item as $item) { ?>

                                        <tr>
                                            <td class="text-center"><?= $item->product_name; ?></td>
                                            <td class="text-right">P<?= number_format($item->product_price, 2); ?></td>
                                            <td class="text-center"><?= $item->product_quantity; ?></td>
                                            <td class="text-right">P<?= number_format($item->product_price *  $item->product_quantity, 2); ?></td>
                                        </tr>
                                        
                                    <?php 
                                $totalAmount += $item->product_price *  $item->product_quantity;
                                } ?>
                                    <tr>
                                    <td></td>
                                    <td></td>
                                    <td class="text-center"><strong>Total</strong></td>
                                    <td class="text-right"><strong>P<?=number_format($totalAmount, 2); ?></strong></td>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>

</html>
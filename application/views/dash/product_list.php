<?php
defined('BASEPATH') or exit('No direct script access allowed');
if (!$_SESSION['email']) {
    redirect('home', 'refresh');
}
?>
<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title>Hello, world!</title>
</head>

<body>
    <?php $this->load->view('inc/nav'); ?>

    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <!-- sidebar -->
                <?php $this->load->view('inc/sidebar'); ?>
            </div>
            <div class="col-md-9">
                <table class="table mt-3">
                    <thead>
                        <tr>
                            <th scope="col" class="text-center">ID</th>
                            <th scope="col" class="text-center">Name</th>
                            <th scope="col" class="text-center">Price</th>
                            <th scope="col" class="text-center">Quantity</th>
                            <th scope="col" class="text-center">Edit</th>
                            <th scope="col" class="text-center">Delete</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $this->db->order_by("product_id", "asc");
                        $product_list = $this->db->get('products');
                        foreach($product_list->result() as $product){ ?>
                        <tr>
                            <th scope="row" class="text-center"><?=$product->product_id;?></th>
                            <td class="text-center"><?=$product->product_name;?></td>
                            <td class="text-right">P<?=number_format($product->product_price, 2);?></td>
                            <td class="text-center"><?=$product->product_quantity;?></td>
                            <td class="text-center"><a href="<?=site_url();?>/productController/update_product/<?=$product->product_id;?>">Edit</a></td>
                            <td class="text-center"><a href="<?=site_url();?>/productController/delete_product/<?=$product->product_id;?>">Delete</a></td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>

</html>
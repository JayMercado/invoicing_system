<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dash extends CI_Controller {
    public function index(){
        if ($this->input->post('filter_by_date')) {
            $data = [
                'start_date' => $this->input->post('start_date'),
                'end_date' => $this->input->post('end_date')
            ];
            $this->load->view('dash/dash_home', $data);
        } else {
            $this->load->view('dash/dash_home');
        }
    }
}
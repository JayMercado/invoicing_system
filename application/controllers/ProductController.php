<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ProductController extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Product');
    }
    public function index()
    {
        $this->load->view('dash/add_product');
    }
    public function view_products()
    {
        $this->load->view('dash/product_list');
    }
    public function add_product()
    {
        if ($this->input->post('add_product')) {
            $product_name = $this->input->post('product_name');
            $product_price = $this->input->post('product_price');
            $product_quantity = $this->input->post('product_quantity');
            $product_data = [
                'product_name' => $product_name,
                'product_price' => $product_price,
                'product_quantity' => $product_quantity,
            ];
            $this->Product->add_product($product_data);
            redirect('ProductController/view_products', 'refresh');
        }
    }

    public function update_product($product_id) {
        $this->load->view('dash/update_product', $product_id);
    }

    public function update_product_process($product_id){
        if($this->input->post('update_product')){
            $product_name = $this->input->post('product_name');
            $product_price = $this->input->post('product_price');
            $product_quantity = $this->input->post('product_quantity');
            $product_details = [
                'product_name' => $product_name,
                'product_price' => $product_price,
                'product_quantity' => $product_quantity
            ];
            $this->db->where('product_id', $product_id);
            $this->db->update('products', $product_details);
            redirect('ProductController/view_products', 'refresh');
        }
    }

    public function delete_product($product_id) {
        $this->db->where('product_id', $product_id);
        $this->db->delete('products');
        redirect('ProductController/view_products', 'refresh');
    }
}

<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('User');
    }

    public function index()
    {
        $this->load->view('inc/header');
        $this->load->view('home');
        $this->load->view('inc/footer');
    }

    public function login()
    {
        $this->load->view('inc/header');
        $this->load->view('login');
        $this->load->view('inc/footer');
    }

    public function register()
    {
        $this->load->view('inc/header');
        $this->load->view('register');
        $this->load->view('inc/footer');
    }

    public function error()
    {
        $this->load->view('inc/header');
        $this->load->view('error');
        $this->load->view('inc/footer');
    }

    public function login_process()
    {
        if ($this->input->post('login')) {
            $email = $this->input->post('email');
            $password = md5($this->input->post('password'));
            $user_data = [
                'email' => $email,
                'password' => $password
            ];
            $user_list = $this->db->get_where('users', array('email' => $user_data['email']));
            if ($user_list) {
                foreach ($user_list->result() as $user) {
                    if ($user_data['email'] == $user->email && $user_data['password'] == $user->password) {
                        $_SESSION['email'] = $user_data['email'];
                        redirect('dash', 'refresh');
                    } else {
                        redirect('home/error', 'refresh');
                    }
                }
            } else {
                redirect('home/error', 'refresh');
            }
        } else {
            redirect('home/error', 'refresh');
        }
    }

    public function register_process()
    {
        if ($this->input->post('register')) {
            $email = $this->input->post('email');
            $password = md5($this->input->post('password'));
            $user_data = [
                'email' => $email,
                'password' => $password
            ];
            $this->User->register_user($user_data);
            $user_list = $this->db->get_where('users', array('email' => $user_data['email']));
            foreach ($user_list->result() as $user) {
                if ($user_data['email'] == $user->email && $user_data['password'] == $user->password) {
                    $_SESSION['email'] = $user_data['email'];
                    redirect('dash', 'refresh');
                }
            }
            redirect('home', 'refresh');
        } else {
            redirect('home', 'refresh');
        }
    }

    public function logout()
    {
        session_unset();
        session_destroy();
        redirect('home', 'refresh');
    }
}

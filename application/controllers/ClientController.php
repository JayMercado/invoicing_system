<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ClientController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Client');
    }

    public function index()
    {
        $this->load->view('dash/add_client');
    }

    public function view_clients()
    {
        $this->load->view('dash/client_list');
    }

    public function add_client()
    {
        if ($this->input->post('add_client')) {
            $client_name = $this->input->post('client_name');
            $contact_number = $this->input->post('contact_number');
            $address = $this->input->post('address');
            $client_data = [
                'client_name' => $client_name,
                'contact_number' => $contact_number,
                'address' => $address
            ];
            $this->Client->add_client($client_data);
            redirect('ClientController/view_clients', 'refresh');
        }
    }

    public function update_client($client_id) {
        $this->load->view('dash/update_client', $client_id);
    }

    public function update_client_process($client_id){
        if($this->input->post('update_client')){
            $client_name = $this->input->post('client_name');
            $contact_number = $this->input->post('contact_number');
            $address = $this->input->post('address');
            $client_details = [
                'client_name' => $client_name,
                'contact_number' => $contact_number,
                'address' => $address
            ];
            $this->db->where('client_id', $client_id);
            $this->db->update('clients', $client_details);
            redirect('ClientController/view_clients', 'refresh');
        }
    }

    public function delete_client($client_id) {
        $this->db->where('client_id', $client_id);
        $this->db->delete('clients');
        redirect('ClientController/view_clients', 'refresh');
    }
}

<?php
defined('BASEPATH') or exit('No direct script access allowed');

class InvoiceController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Invoice');
        $this->load->model('LineItem');
        $this->load->helper('url');
    }

    public function index()
    {
        $this->load->view('dash/add_invoice');
    }

    public function view_invoices()
    {
        $this->load->view('dash/invoice_list');
    }

    public function view_invoice($invoice_id)
    {
        $this->load->view('dash/invoice', $invoice_id);
    }

    public function add_invoice()
    {
        if ($this->input->post('add_invoice')) {

            $invoice_client = $this->input->post('invoice_client');
            $invoice_payment_due = $this->input->post('invoice_payment_due');
            $invoice_date = date('Y-m-d');
            $invoice_data = [
                'invoice_client' => $invoice_client,
                'invoice_date' => $invoice_date,
                'invoice_payment_due' => $invoice_payment_due,
                'invoice_status' => 'Unpaid'
            ];

            $invoice_id = $this->Invoice->add_invoice($invoice_data);

            foreach ($this->input->post('line_items') as $lineItem) {

                $this->db->where('product_name', $lineItem['product_name']);
                $product_dtls = $this->db->get('products');
                $prdct_q = $product_dtls->result();
                foreach ($prdct_q as $prdct) {
                    $product_id = $prdct->product_id;
                    $product_name = $prdct->product_name;
                    $product_price = $prdct->product_price;
                    $product_quantity = $prdct->product_quantity;
                }

                $line_data = [
                    'invoice_id' => $invoice_id,
                    'product_id' => $product_id,
                    'product_name' => $product_name,
                    'product_price' => $product_price,
                    'product_quantity' => $lineItem['quantity'],
                ];

                $up_to_date_product_quantity = $product_quantity - $lineItem['quantity'];

                $up_to_date_product_details = [
                    'product_quantity' => $up_to_date_product_quantity
                ];
                $this->LineItem->add_item($line_data);

                $this->db->where('product_id', $product_id);
                $this->db->update('products', $up_to_date_product_details);
            }
        }
    }

    public function update_invoice($invoice_id)
    {
        $this->load->view('dash/update_invoice', $invoice_id);
    }

    public function update_invoice_process($invoice_number)
    {
        if ($this->input->post('update_invoice')) {
            $invoice_client = $this->input->post('invoice_client');
            $invoice_payment_due = $this->input->post('invoice_payment_due');
            $invoice_status = $this->input->post('invoice_status');
            $invoice_details = [
                'invoice_client' => $invoice_client,
                'invoice_payment_due' => $invoice_payment_due,
                'invoice_status' => $invoice_status
            ];
            $this->db->where('invoice_number', $invoice_number);
            $this->db->update('invoices', $invoice_details);
            redirect('InvoiceController/view_invoices', 'refresh');
        }
    }

    public function delete_invoice($invoice_number)
    {
        $this->db->where('invoice_number', $invoice_number);
        $this->db->delete('invoices');
        $this->db->where('invoice_id', $invoice_number);
        $this->db->delete('line_items');
        redirect('InvoiceController/view_invoices', 'refresh');
    }
}

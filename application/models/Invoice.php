<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Invoice extends CI_Model {
    public function add_invoice($invoice_details){
        $this->db->insert('invoices', $invoice_details);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }
}
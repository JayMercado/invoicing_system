<?php
defined('BASEPATH') or exit('No direct script access allowed');


class LineItem extends CI_Model
{
    public function add_item($line_data)
    {
        $this->db->insert('line_items', $line_data);
    }
}
